package com.getjavajob.training.zaytsevi.mailer.service;

import com.getjavajob.training.zaytsevi.mailer.model.MailerEvent;
import com.getjavajob.training.zaytsevi.mailer.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void createNewEvent(MailerEvent messageEvent) {
        messageRepository.save(messageEvent);
    }
}
