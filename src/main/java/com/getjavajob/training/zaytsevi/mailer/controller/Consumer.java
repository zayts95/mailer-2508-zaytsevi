package com.getjavajob.training.zaytsevi.mailer.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.training.zaytsevi.mailer.model.MailerEvent;
import com.getjavajob.training.zaytsevi.mailer.service.GmailService;
import com.getjavajob.training.zaytsevi.mailer.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public class Consumer {

    final MessageService messageService;
    final GmailService gmailService;

    @Autowired
    public Consumer(MessageService messageService, GmailService gmailService) {
        this.messageService = messageService;
        this.gmailService = gmailService;
    }

    @JmsListener(destination = "mailer-queue")
    public void consumeMessage(String jsonMessage){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            MailerEvent mailerEvent = objectMapper.readValue(jsonMessage, MailerEvent.class);
            messageService.createNewEvent(mailerEvent);
            gmailService.sendMessage(mailerEvent);
        } catch (JsonProcessingException | MessagingException e) {
            e.printStackTrace();
        }
    }
}
