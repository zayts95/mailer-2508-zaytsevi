package com.getjavajob.training.zaytsevi.mailer.repository;

import com.getjavajob.training.zaytsevi.mailer.model.MailerEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CrudRepository<MailerEvent, String> {
}
