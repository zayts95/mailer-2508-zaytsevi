package com.getjavajob.training.zaytsevi.mailer.service;

import com.getjavajob.training.zaytsevi.mailer.model.MailerEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class GmailService {

    public static final String SUBJECT = "Notification";
    public static final String TEXT_FOR_POST = " posted something on your page";
    public static final String TEXT_FOR_FRIEND_REQUEST = " sent you a friend request";

    private final String email;
    private final Session session;

    @Autowired
    public GmailService(@Value("${gmail.email}") String email, @Value("${gmail.password}")String password, @Value("${gmail.host}")String host) {
        this.email = email;
        Properties properties = System.getProperties();

        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        session = Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });
    }

    public void sendMessage(MailerEvent mailerEvent) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(email));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailerEvent.getReceiverEmail()));
        message.setSubject(SUBJECT);
        if (mailerEvent.getType() == MailerEvent.Type.MESSAGE){
            message.setText(mailerEvent.getSenderName() + " " + mailerEvent.getSenderSurname() + TEXT_FOR_POST);
        } else if (mailerEvent.getType() == MailerEvent.Type.FRIEND_REQUEST){
            message.setText(mailerEvent.getSenderName() + " " + mailerEvent.getSenderSurname() + TEXT_FOR_FRIEND_REQUEST);
        }
        Transport.send(message);
    }
}
